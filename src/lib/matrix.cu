#include <iostream>
#include <chrono>
#include "matrix.h"

#define HANDLE_CUDA_ERROR(x) { \
    const auto err = x; \
    if(err != cudaSuccess) { printError(cudaGetErrorString(err), __FILE__, __LINE__); } \
}

#define CHECK_KERNEL_ERROR() { \
    cudaError_t err = cudaGetLastError(); \
    if(err != cudaSuccess) { printError(cudaGetErrorString(err), __FILE__, __LINE__); } \
}

namespace
{
    static void printError(const char *message, const char *file, int line) {
        fprintf(stderr, "%s at %s:%d\n", message, file, line);
        exit( EXIT_FAILURE );
    }
}

using namespace std;

struct pivot_data {
    size_t pivotRow, pivotCol, choosenRow;
    float pivotValue;
    unsigned busy; // used as a semaphore
};

void printPivot(pivot_data *data) {
    fprintf(stderr, "Pivot data: {%lu, %lu, %lu, %f}\n", data->pivotRow, data->pivotCol, data->choosenRow, data->pivotValue);
}

void std::printMatrix(float *A, size_t n, size_t m) {
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            cerr << A[i * m + j] << " ";
        }
        cerr << endl;
    }
}

__global__ void swapAndNormalize(float *A, size_t rowSize, pivot_data *pivotData);
__global__ void selectPivot(float *A, size_t numRows, size_t rowSize, pivot_data *data);
__global__ void combineRows(float *A, size_t numCols, size_t firstRow, pivot_data *pivotData);

/**
 * Transforms the matrix A into its (unique) reduced row-echelon form.
 * @param A     A pointer in device space to the matrix to be transformed. The result data will be contained here.
 * @param n     The number of rows of the matrix
 * @param m     The number of columns of the matrix
 * @return      The rank of the matrix
*/
__host__ size_t std::reducedRowEchelon(float *A, size_t n, size_t m)
{
    size_t pivotRow = 0, pivotCol = 0;
    constexpr size_t max_block_size = 1024;

    // fprintf(stderr, "Reducing matrix of size %lux%lu\n", n, m);

    pivot_data *data, *data_d;
    cudaHostAlloc(&data, sizeof(pivot_data), cudaHostAllocMapped);
    HANDLE_CUDA_ERROR(cudaHostGetDevicePointer(&data_d, data, 0));

    while(pivotRow < n && pivotCol < m)
    {
        *data = {pivotRow, pivotCol, 0, 0, 0};
        size_t halfRows = (n - pivotRow - 1) / 2 + 1;
        unsigned blocksNum = (halfRows - 1) / max_block_size + 1;
        unsigned threadsNum = (halfRows - 1) / blocksNum + 1;
        // fprintf(stderr, "Computing pivot at (%lu,%lu) (size is %lux%lu) with %u blocks of %u threads\n", pivotRow, pivotCol, n, m, blocksNum, threadsNum);

        // auto start = chrono::high_resolution_clock::now();

        selectPivot<<<blocksNum, threadsNum, threadsNum * (sizeof(float) + sizeof(size_t))>>>(A, n, m, data_d);

        HANDLE_CUDA_ERROR(cudaStreamSynchronize(0));
        // auto stop = chrono::high_resolution_clock::now();
        // auto us = chrono::duration_cast<chrono::microseconds>(stop - start);
        // fprintf(stderr, "Pivot computed in %ldus on %lu rows.\n", us.count(), n - pivotRow);

        // printPivot(data);

        if(data->pivotValue != 0) {
            size_t affectedCols = m - pivotCol;
            blocksNum = (affectedCols - 1) / max_block_size + 1;
            threadsNum = (affectedCols - 1) / blocksNum + 1;
            swapAndNormalize<<<blocksNum, threadsNum>>>(A, m, data_d);

            dim3 blocksNumAbove = dim3(pivotRow, blocksNum);
            dim3 blocksNumBelow = dim3(n - pivotRow - 1, blocksNum);

            HANDLE_CUDA_ERROR(cudaStreamSynchronize(0));

            // Rows above pivot
            if(pivotRow > 0) {
                // fprintf(stderr, "Combining rows above with gridSize (%u, %u) and blockSize %u.\n", blocksNumAbove.x, blocksNumAbove.y, threadsNum);
                combineRows<<<blocksNumAbove, threadsNum>>>(A, m, 0, data_d);
            }
            // Rows below pivot
            if(pivotRow < n - 1) {
                // fprintf(stderr, "Combining rows below with gridSize (%u, %u) and blockSize %u.\n", blocksNumBelow.x, blocksNumBelow.y, threadsNum);
                combineRows<<<blocksNumBelow, threadsNum>>>(A, m, pivotRow + 1, data_d);
            }

            pivotRow ++;
            
            HANDLE_CUDA_ERROR(cudaStreamSynchronize(0));
            // float *tmp_matrix = (float*) malloc(n * m * sizeof(float));
            // cudaMemcpy(tmp_matrix, A, n * m * sizeof(float), cudaMemcpyDeviceToHost);
            // cerr << "Temp matrix:\n";
            // printMatrix(tmp_matrix, n, m);
        }
        
        pivotCol ++;
    }

    return pivotRow;
}

/**
 * Swap the choosen pivot row with the current one and ormalize it by dividing by the pivot value. In this way the pivot value will be 1.
 * One thread per column (use more blocks if there are more than 1024 columns)
 * @param A             A pointer in device space to the matrix
 * @param rowSize       The number of columns in the matrix
 * @param pivotData     A pointer in device space to a pivot_data structure produced by selectPivot
 */
__global__ void swapAndNormalize(float *A, size_t rowSize, pivot_data *pivotData)
{
    size_t pivotRow = pivotData->pivotRow;
    size_t choosenRow = pivotData->choosenRow;
    int col = blockIdx.x * blockDim.x + threadIdx.x + pivotData->pivotCol;
    if(col < rowSize)
    {
        size_t index = pivotRow * rowSize + col;
        if(pivotRow != choosenRow) {
            float tmp = A[index];
            A[index] = A[choosenRow * rowSize + col];
            A[choosenRow * rowSize + col] = tmp;
        }
        A[index] /= pivotData->pivotValue;
    }
}

/**
 * Select the pivot for the j-th column of A from the i-th row onwards. Such parameters must be provided in the in-out param pivotData.
 * The pivotRow and pivotColumn fields of pivotData must be set to the input values i and j. Moreover the busy field should be set to 0 since it represents a semaphore to avoid race conditions when saving the output.
 * One thread per two rows (using blocks if there are more than 1024 rows).
 * @param A             A pointer in device space to the matrix
 * @param numRows       The number of rows in the matrix
 * @param rowSize       The number of columns in the matrix
 * @param pivotData     A pointer in device space to a pivot_data structure used for input/output
 */
__global__ void selectPivot(float *A, size_t numRows, size_t rowSize, pivot_data *data)
{
    extern __shared__ float shared_memory[];
    size_t *rowIndexes = (size_t*) shared_memory;
    float *elements = (shared_memory + blockDim.x * (sizeof(size_t) / sizeof(float)));

    size_t r1 = blockIdx.x * 2 * blockDim.x + threadIdx.x + data->pivotRow;
    size_t r2 = r1 + blockDim.x;
    r2 -= (r2 >= numRows) * blockDim.x; // Fast modulo

    // printf("Thread %04u chooses between position %lu and %lu.\n", threadIdx.x, r1, r2);
    // __syncthreads(); // DEBUG

    // Set data in working memory
    /* Since the data is in row-major order and the rowSize is usually
       a multiple of a power of 2 this will probably create sequential
       access to the memory.
       Probably dealigning the rows is a solution but idk. */
    float v1 = A[r1 * rowSize + data->pivotCol];
    float v2 = A[r2 * rowSize + data->pivotCol];

    int m = abs(v2) > abs(v1);
    elements[threadIdx.x] = v2 * m + v1 * (1 - m);
    rowIndexes[threadIdx.x] = r2 * m + r1 * (1 - m);
    // printf("Thread %04u selected position %lu(%f).\n", threadIdx.x, rowIndexes[threadIdx.x], elements[threadIdx.x]);
    __syncthreads();

    // Perform a reduced max
    size_t s = (blockDim.x - 1) / 2 + 1;
    while(s > 0) {
        // if(threadIdx.x == 0) {
        //     printf("--- NEW ITERATION (s=%lu) ---\n", s);
        // }
        // __syncthreads(); // DEBUG
        if (threadIdx.x < s) {
            size_t i1 = rowIndexes[threadIdx.x];
            size_t second_item = threadIdx.x + s;
            second_item -= (second_item >= blockDim.x) * s;
            size_t i2 = rowIndexes[second_item];
            float v1 = elements[threadIdx.x];
            float v2 = elements[second_item];

            // printf("Thread %u chooses between position %lu and %lu.\n", threadIdx.x, i1, i2);
            // __syncthreads(); // DEBUG

            int m = abs(v2) > abs(v1);
            elements[threadIdx.x] = v2 * m + v1 * (1 - m);
            rowIndexes[threadIdx.x] = i2 * m + i1 * (1 - m);
            // printf("Thread %u selected position %lu(%f).\n", threadIdx.x, rowIndexes[threadIdx.x], elements[threadIdx.x]);
            // __syncthreads(); // DEBUG
        }
        size_t inc = 1 * (s > 1);
        s = (s - 1) / 2 + inc;
        __syncthreads();
    }

    // Each block "leader" updates the result in the result data structure
    if(threadIdx.x == 0) {
        while(atomicExch(&data->busy, 1) == 1) {} // Wait for the struct to be free
        // Save the result (if needed)
        if(abs(data->pivotValue) < abs(elements[0])) {
            data->pivotValue = elements[0];
            data->choosenRow = rowIndexes[0];
        }
        atomicExch(&data->busy, 0); // Free the semaphore
    }
}

/**
 * Combine the rows below the pivot with the pivot row so that the pivot column has 0s below the pivot.
 * One thread per column (using blocks if there are more than 1024 rows). The row delta is specified with the blockIdx.y value.
 * @param A             A pointer in device space to the matrix
 * @param rowSize       The number of columns in the matrix
 * @param firstRow      The first row in the kernel
 * @param pivotData     A pointer in device space to a \ref{pivot_data} structure produce by selectPivot
 */
__global__ void combineRows(float *A, size_t numCols, size_t firstRow, pivot_data *pivotData)
{
    size_t pivotRow = pivotData->pivotRow;
    size_t pivotCol = pivotData->pivotCol;
    size_t row = firstRow + blockIdx.x;
    size_t col = pivotCol + threadIdx.x + blockIdx.y * blockDim.x;

    if(col < numCols)
    {
        // Perform the vector linear combination row = row - row[0] * pivotRow
        // Doing that brings the element in pivotCol to 0
        float factor = A[row * numCols + pivotCol];
        A[row * numCols + col] -= A[pivotRow * numCols + col] * factor;
    }
}

/**
 * Perform the rank decomposition (in row-major order) A = C * F.
 * 
 * @param A     A pointer in device space to the n*m matrix to be factorized
 * @param n     The number of rows of A
 * @param m     The number of columns of A
 * @param C     A pointer in host space where the n*r matrix C will be stored
 * @param F     A pointer in host space where the r*m matrix F will be stored
 * 
 * @returns The rank r of the matrix A.
 */
__host__ size_t std::rankFactorization(float *A, size_t n, size_t m, float *&C, float *&F)
{
    size_t size = n * m * sizeof(float);
    float *A_h = (float*) malloc(size);
    cudaMemcpyAsync(A_h, A, size, cudaMemcpyDeviceToHost);

    // cudaStreamSynchronize(0);
    // fprintf(stderr, "Original matrix:\n");
    // printMatrix(A_h, n, m);

    // Compute the reduced row-echelon form of A
    size_t r = reducedRowEchelon(A, n, m);
    // Store it in B
    float *B = (float*) malloc(size);
    cudaMemcpy(B, A, size, cudaMemcpyDeviceToHost);

    // fprintf(stderr, "Reduced row-echelon (rank %lu):\n", r);
    // printMatrix(B, n, m);

    C = (float*) malloc(sizeof(float) * n * r);
    F = (float*) malloc(sizeof(float) * m * r);

    // cerr << "Copying C...\n";

    int i = 0, j = 0;
    // C is made of the pivot columns of B
    while(i < r)
    {
        while(B[i * m + j] == 0)
        {
            j++;
        }
        // for(int k = 0; k < n; k++)
        // {
        //     C[k * r + i] = A[k * m + j];
        // }
        cudaMemcpy2D(&C[i], r * sizeof(float), &A_h[j], m * sizeof(float), sizeof(float), n, cudaMemcpyHostToHost);
        i++;
        j++;
    }

    // cerr << "C copied\n";

    // F is made of the first r rows of B
    // Maybe this can be done without copying with just a realloc
    memcpy(F, B, sizeof(float) * m * r);
    free(B);
    free(A_h);
    // cerr << "FACTORIZATION DONE\n";

    return r;
}

// TODO this can be done better frfr
__global__ void std::vectorSum(void* v, int n, void* result)
{
    float* C = (float*) result;
    float* vec = (float*) v;
    *C = 0;
    for(int i = 0; i < n; i++) {
        *C += vec[i];
    }
}
