#include "tensor.h"
#include "matrix.h"
#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <sstream>

// Handle cuTENSOR errors
#define HANDLE_CUTENSOR_ERROR(x) { \
    const auto err = x; \
    if( err != CUTENSOR_STATUS_SUCCESS ) { \
        fprintf(stderr, "%s\n", cudaGetErrorString(cudaGetLastError())); \
        printError(cutensorGetErrorString(err), __FILE__, __LINE__); \
    } \
}

#define HANDLE_CUDA_ERROR(x) { \
  const auto err = x; \
  if( err != cudaSuccess ) \
  { printError(cudaGetErrorString(err), __FILE__, __LINE__); } \
}

namespace
{
    static void printError(const char *message, const char *file, int line) {
        fprintf(stderr, "%s at %s:%d\n", message, file, line);
        exit( EXIT_FAILURE );
    }

    // void printStats()
    // {
    //     int num_gpus;
    //     size_t free, total;
    //     cudaGetDeviceCount( &num_gpus );
    //     for ( int gpu_id = 0; gpu_id < num_gpus; gpu_id++ ) {
    //         cudaSetDevice( gpu_id );
    //         int id;
    //         cudaGetDevice( &id );
    //         cudaMemGetInfo( &free, &total );
    //         fprintf(stderr, "GPU %d memory: free=%lu total=%lu\n", id, free, total);
    //     }
    // }
}

using namespace std;

const cutensorDataType_t Tensor::inputType = CUTENSOR_R_32F;
const cutensorComputeDescriptor_t Tensor::computeType = CUTENSOR_COMPUTE_DESC_32F;
cutensorHandle_t Tensor::handle;

Tensor::Tensor()
{
    host_data = NULL;
}

Tensor::Tensor(const vector<int>& modes, const vector<int64_t>& modes_extents)
{
    if(modes.size() != modes_extents.size())
    {
        throw logic_error("Modes and extents vector must have the same size.");
    }
    this->modes = modes;
    this->modes_extents = modes_extents;
    host_data = (float*) malloc(getDataSize());
    if(host_data == NULL)
    {
        stringstream ss;
        ss << "Unable to allocate " << (float(getDataSize()) / 1024 / 1024) << "MB of memory";
        throw runtime_error(ss.str());
    }
}

vector<int> Tensor::getModes() const
{
    return modes;
}

int Tensor::getMode(size_t i) const
{
    return modes[i];
}

vector<int64_t> Tensor::getModesExtents() const
{
    return modes_extents;
}

int64_t Tensor::getModeExtent(size_t i) const
{
    return modes_extents[i];
}

size_t Tensor::getDataSize() const
{
    return sizeof(float) * getElementsCount();
}

size_t Tensor::getElementsCount() const
{
    size_t count = 1;
    for(auto ex : modes_extents)
    {
        count *= ex;
    }
    return count;
}

size_t Tensor::getModesCount() const
{
    return modes.size();
}

void Tensor::getData(float *&buff) const
{
    size_t size = getDataSize();
    buff = (float*) realloc(buff, size);
    memcpy(buff, host_data, size);
}

Tensor::Tensor(const vector<int>& modes, const vector<int64_t>& modes_extents, const float *data) :
    Tensor(modes, modes_extents)
{
    initData(data);
}

Tensor::Tensor(const Tensor& other) :
    Tensor()
{
    *this = other;
}

Tensor::Tensor(Tensor&& other) :
    Tensor()
{
    *this = move(other);
}

Tensor& Tensor::operator=(const Tensor& other)
{
    modes = other.modes;
    modes_extents = other.modes_extents;
    host_data = (float*) realloc(host_data, other.getDataSize());
    initData(other.host_data);

    return *this;
}

Tensor& Tensor::operator=(Tensor&& other)
{
    modes = move(other.modes);
    modes_extents = move(other.modes_extents);
    free(host_data);
    host_data = other.host_data;
    other.host_data = NULL;

    return *this;
}

void Tensor::initData(const float *data)
{
    size_t dataSize = getDataSize();
    memcpy(host_data, data, dataSize);
}

Tensor::~Tensor()
{
    free(host_data);
    host_data = NULL;
}

float Tensor::asScalar() const
{
    if(modes.size() > 0)
    {
        throw runtime_error("Tensor is not a scalar.");
    }
    else
    {
        return host_data[0];
    }
}

cutensorTensorDescriptor_t Tensor::getDescriptor(const cutensorHandle_t& handle) const
{
    cutensorTensorDescriptor_t descriptor;
    HANDLE_CUTENSOR_ERROR( cutensorCreateTensorDescriptor(
        handle, &descriptor,
        modes_extents.size(), modes_extents.data(),
        NULL, inputType, 128) );
    return descriptor;
}

vector<int> Tensor::getCombinedModes(const Tensor& A, const Tensor& B)
{
    vector<int> modes;

    for(int i = 0; i < A.modes.size(); i++)
    {
        if(find(B.modes.begin(), B.modes.end(), A.modes[i]) == B.modes.end())
        {
            modes.push_back(A.modes[i]);
        }
    }
    for(int i = 0; i < B.modes.size(); i++)
    {
        if(find(A.modes.begin(), A.modes.end(), B.modes[i]) == A.modes.end())
        {
            modes.push_back(B.modes[i]);
        }
    }

    return modes;
}

Tensor Tensor::combine(const Tensor& A, const Tensor& B)
{
    vector<int> modes;
    vector<int64_t> modes_extents;

    for(int i = 0; i < A.modes.size(); i++)
    {
        if(find(B.modes.begin(), B.modes.end(), A.modes[i]) == B.modes.end())
        {
            modes.push_back(A.modes[i]);
            modes_extents.push_back(A.modes_extents[i]);
        }
    }
    for(int i = 0; i < B.modes.size(); i++)
    {
        if(find(A.modes.begin(), A.modes.end(), B.modes[i]) == A.modes.end())
        {
            modes.push_back(B.modes[i]);
            modes_extents.push_back(B.modes_extents[i]);
        }
    }

    Tensor result(modes, modes_extents);
    combine(A, B, result);
    return result;
}

void Tensor::combine(const Tensor& A, const Tensor& B, Tensor& C)
{
    void* C_d = combine(A, B, C.modes, C.modes_extents);
    cudaMemcpy(C.host_data, C_d, C.getDataSize(), cudaMemcpyDeviceToHost);
    cudaFree(C_d);
}

void* Tensor::combine(const Tensor& A, const Tensor& B, vector<int>& modesC, vector<int64_t>& extentsC)
{
    if(Tensor::handle == NULL)
    {
        throw runtime_error("The default handle must be initialized before being used.");
    }
    return combine(Tensor::handle, A, B, modesC, extentsC);
}

void* Tensor::combine(const cutensorHandle_t& handle, const Tensor& A, const Tensor& B, vector<int>& modesC, vector<int64_t>& extentsC)
{
    bool empty_flag = modesC.size() == 0;

    // cerr << "Multiplying tensors:\n" << A << "and\n" << B;

    // fprintf(stderr, "Combining tensors with %lu modes:\n", A.getModesCount());
    // for(size_t i = 0; i < A.getModesCount(); i++) {
    //     fprintf(stderr, " %d(%ld)", A.getMode(i), A.getModeExtent(i));
    // }
    // cerr << endl;
    // fprintf(stderr, "And %lu modes:\n", B.getModesCount());
    // for(size_t i = 0; i < B.getModesCount(); i++) {
    //     fprintf(stderr, " %d(%ld)", B.getMode(i), B.getModeExtent(i));
    // }
    // cerr << endl;
    // fprintf(stderr, "Into a tensor with %lu modes:\n", modesC.size());
    // for(size_t i = 0; i < modesC.size(); i++) {
    //     fprintf(stderr, " %d(%ld)", modesC[i], extentsC[i]);
    // }
    // cerr << endl;

    if(empty_flag)
    {
        if(A.modes.size() != 0)
        {
            modesC.push_back(A.modes[0]);
            extentsC.push_back(A.modes_extents[0]);
        }
        else if(B.modes.size() != 0)
        {
            modesC.push_back(B.modes[0]);
            extentsC.push_back(B.modes_extents[0]);
        }
        else
        {
            float result = A.host_data[0] * B.host_data[0];
            void* C_d;
            cudaMalloc(&C_d, sizeof(float));
            cudaMemcpy(C_d, &result, sizeof(float), cudaMemcpyHostToDevice);
            return C_d;
        }
    }

    size_t sizeC = sizeof(float);
    for(auto ex : extentsC)
    {
        sizeC *= ex;
    }
    cutensorTensorDescriptor_t descriptorC;
    cutensorTensorDescriptor_t descriptorA = A.getDescriptor(handle);
    cutensorTensorDescriptor_t descriptorB = B.getDescriptor(handle);
    // fprintf(stderr, "Extents of C (%p):", extentsC.data());
    // for(auto e : extentsC) fprintf(stderr, " %ld", e);
    // fprintf(stderr, "\n");

    HANDLE_CUTENSOR_ERROR( cutensorCreateTensorDescriptor(
        handle, &descriptorC,
        extentsC.size(), extentsC.data(),
        NULL, inputType, 128) );

    // printStats();
    void *A_d, *B_d, *C_d;

    // fprintf(stderr, "Allocating %lu, %lu and %lu Bytes of VRAM.\n", A.getDataSize(), B.getDataSize(), sizeC);
    // fprintf(stderr, "Tensor orders are %lu, %lu and %lu.\n", A.getModesCount(), B.getModesCount(), modesC.size());

    cudaMalloc(&A_d, A.getDataSize());
    cudaMalloc(&B_d, B.getDataSize());
    cudaMalloc(&C_d, sizeC);

    cudaMemcpy(A_d, A.host_data, A.getDataSize(), cudaMemcpyHostToDevice);
    cudaMemcpy(B_d, B.host_data, B.getDataSize(), cudaMemcpyHostToDevice);

    cutensorOperationDescriptor_t desc;
    HANDLE_CUTENSOR_ERROR( cutensorCreateContraction(
        handle,
        &desc,
        descriptorA, A.modes.data(), CUTENSOR_OP_IDENTITY,
        descriptorB, B.modes.data(), CUTENSOR_OP_IDENTITY,
        descriptorC, modesC.data(), CUTENSOR_OP_IDENTITY,
        descriptorC, modesC.data(),
        computeType) );
    
    // fprintf(stderr, "Descriptor pointer is %p.\n", desc);

    cutensorPlanPreference_t planPref;
    HANDLE_CUTENSOR_ERROR(cutensorCreatePlanPreference(
        handle,
        &planPref,
        CUTENSOR_ALGO_DEFAULT,
        CUTENSOR_JIT_MODE_NONE));

    // fprintf(stderr, "Plan preference pointer is %p.\n", planPref);

    uint64_t workspaceSizeEstimate = 0;
    const cutensorWorksizePreference_t workspacePref = CUTENSOR_WORKSPACE_MAX;
    HANDLE_CUTENSOR_ERROR(cutensorEstimateWorkspaceSize(handle,
        desc,
        planPref,
        workspacePref,
        &workspaceSizeEstimate));
    
    // fprintf(stderr, "Workspace estimate is %lu.\n", workspaceSizeEstimate);

    cutensorPlan_t plan;
    HANDLE_CUTENSOR_ERROR(cutensorCreatePlan(handle,
        &plan,
        desc,
        planPref,
        workspaceSizeEstimate));

    // fprintf(stderr, "Plan pointer is %p.\n", plan);

    // query actually used workspace
    uint64_t actualWorkspaceSize = 0;
    HANDLE_CUTENSOR_ERROR(cutensorPlanGetAttribute(handle,
        plan,
        CUTENSOR_PLAN_REQUIRED_WORKSPACE,
        &actualWorkspaceSize,
        sizeof(actualWorkspaceSize)));

    // fprintf(stderr, "Actual workspace is %lu.\n", workspaceSizeEstimate);

    // At this point the user knows exactly how much memory is need by the operation and
    // only the smaller actual workspace needs to be allocated
    if(actualWorkspaceSize > workspaceSizeEstimate)
    {
        throw runtime_error("Requested workspace is bigger than estimate.");
    }

    void *work = nullptr;
    if (actualWorkspaceSize > 0)
    {
        HANDLE_CUDA_ERROR(cudaMalloc(&work, actualWorkspaceSize));
        if(uintptr_t(work) % 128 != 0) // workspace must be aligned to 128 byte-boundary
        {
            throw runtime_error("Workspace must be aligned to 128 bytes.");
        }
    }

    float alpha = 1, beta = 0;

    HANDLE_CUTENSOR_ERROR(cutensorContract(handle,
        plan,
        (void*) &alpha, A_d, B_d,
        (void*) &beta,  C_d, C_d,
        work, actualWorkspaceSize, 0));

    cudaStreamSynchronize(0);

    // Free used memory

    cutensorDestroyTensorDescriptor(descriptorA);
    cutensorDestroyTensorDescriptor(descriptorB);
    cutensorDestroyTensorDescriptor(descriptorC);
    cutensorDestroyOperationDescriptor(desc);
    cutensorDestroyPlanPreference(planPref);
    cutensorDestroyPlan(plan);

    cudaFree(A_d);
    cudaFree(B_d);
    if(work != NULL) {
        cudaFree(work);
    }

    if(empty_flag)
    {
        void* result;
        cudaMalloc(&result, sizeof(float));
        // TODO parallelize sum
        vectorSum<<<1,1>>>(C_d, extentsC[0], result);
        modesC.clear();
        extentsC.clear();
        cudaStreamSynchronize(0);
        cudaFree(C_d);
        return result;
    }

    return C_d;
}

Tensor Tensor::operator*(const Tensor& other) const
{
    return combine(*this, other);
}

ostream& std::operator<< (ostream& out, const Tensor& t)
{
    out << "Modes:";
    for(int i = 0; i < t.modes.size(); i++) {
        out << " " << t.modes[i] << "(" << t.modes_extents[i] << ")";
    }
    out << endl;

    for(int i = 0; i < t.getElementsCount(); i++) {
        out << t.host_data[i] << std::endl;
    }
    
    return out;
}
