#include "tensor_train.h"
#include "matrix.h"
#include "exception.h"
#include <algorithm>
#include <unordered_set>
#include <pthread.h>
#include <chrono>

using namespace std;

namespace
{
    int linkModeCounter = -1;
}

/**
 * Swap two consecutive tensors in the tensor train. Starting with two Tensors A and B such that A*B=C,
 * modify them by swapping their inner modes, producung two tensors A' (with B's inner modes) an B' (with A's ineer modes)
 * such that A'*B'=C still holds.
 * 
 * @param A     A reference to the first tensor. It is modified to be the second tensor after the swap (so it still has A's inner modes)
 * @param B     A reference to the second tensor. It is modified to be the first tensor (having B's inner modes)
 */
void TensorTrain::swap(Tensor &A, Tensor &B)
{
    vector<int> modesC, modesA, modesB;
    vector<int64_t> modes_extents_C, modes_extents_A, modes_extents_B;

    // A's modes are something like i(a_modes)j
    // B's modes are something like j(b_modes)k

    // I ask cuTENSOR to compute their "product" C with modes i(b_modes)(a_modes)k
    size_t elementsB = 1;
    size_t first_mode_A = 0;
    if(A.getModesCount() > 0 && A.getMode(0) <= 0) {
        int mode = A.getMode(0);
        int64_t extent = A.getModeExtent(0);
        modesC.push_back(mode);
        modes_extents_C.push_back(extent);
        modesB.push_back(mode);
        modes_extents_B.push_back(extent);
        elementsB = extent;
        first_mode_A = 1;
    }

    size_t nB = B.getModesCount();
    size_t last_mode_B = nB;
    if(nB > 0 && B.getMode(nB - 1) <= 0) {
        last_mode_B = nB - 1;
    }

    for(int i = 1; i < last_mode_B; i++) {
        int mode = B.getMode(i);
        int64_t extent = B.getModeExtent(i);
        modesC.push_back(mode);
        modes_extents_C.push_back(extent);
        modesB.push_back(mode);
        modes_extents_B.push_back(extent);
        elementsB *= extent;
    }

    if(nB > 0) {
        modesB.push_back(B.getMode(0));
        modesA.push_back(B.getMode(0));
        modes_extents_A.push_back(0);  // Dummy placeholder
    }

    size_t nA = A.getModesCount();
    size_t elementsA = 1;
    for(int i = first_mode_A; i < nA - 1; i++) {
        int mode = A.getMode(i);
        int64_t extent = A.getModeExtent(i);
        modesC.push_back(mode);
        modes_extents_C.push_back(extent);
        modesA.push_back(mode);
        modes_extents_A.push_back(extent);
        elementsA *= extent;
    }

    if(nB > 0 && last_mode_B < nB) {
        int mode = B.getMode(last_mode_B);
        int64_t extent = B.getModeExtent(last_mode_B);
        modesC.push_back(mode);
        modes_extents_C.push_back(extent);
        modesA.push_back(mode);
        modes_extents_A.push_back(extent);
        elementsA *= extent;
    }

    // cerr << "Modes C: ";
    // for(unsigned i = 0; i < modesC.size(); i++) {
    //     fprintf(stderr, " %d(%ld)", modesC[i], modes_extents_C[i]);
    // }
    // fprintf(stderr, "\nSize is: %lu x %lu\n", elementsA, elementsB);

    // Then this tensor is factorized to produce two tensors with modes
    // i(b_modes)j' and j'(a_modes)d, effectively swapping A and B
    float* tmp_data = (float*) Tensor::combine(A, B, modesC, modes_extents_C);
    // float *matrix_copy = (float*) malloc(elementsA * elementsB * sizeof(float));
    // cudaMemcpy(matrix_copy, tmp_data, elementsA * elementsB * sizeof(float), cudaMemcpyDeviceToHost);
    // cerr << "Original matrix:\n";
    // printMatrix(matrix_copy, elementsA, elementsB);

    float *C, *F;
    int64_t rank = rankFactorization(tmp_data, elementsA, elementsB, C, F);
    if(rank > TensorTrain::maxRank) {
        TensorTrain::maxRank = rank;
    }
    // fprintf(stderr, "Rank is %ld\n", rank);

    if(rank == 0) {
        cerr << endl << " ----- ERROR ----" << endl;
        cerr << "Produced null matrix when multiplying tensors:\n" << A << "and\n" << B;
        cerr << "Sizes were " << elementsA << " and " << elementsB << endl;
        exit(EXIT_FAILURE);
    }

    // cerr << "C:\n";
    // printMatrix(C, elementsA, rank);
    // cerr << "F:\n";
    // printMatrix(F, rank, elementsB);

    modes_extents_A[0] = rank;
    modes_extents_B.push_back(rank);
    // Copy C and F data to A and B tensors
    // Since factorization is row-major but tensors are column-major and M^T = F^T * C^T,
    // if we need M^T = C = B * A then B = F^T and A = C^T (the transposition is implicit in the data usage)

    // cerr << "New modes for A: ";
    // for(unsigned i = 0; i < modesA.size(); i++) {
    //     fprintf(stderr, " %d(%ld)", modesA[i], modes_extents_A[i]);
    // }
    // cerr << endl;
    // cerr << "New modes for B: ";
    // for(unsigned i = 0; i < modesB.size(); i++) {
    //     fprintf(stderr, " %d(%ld)", modesB[i], modes_extents_B[i]);
    // }
    // cerr << endl;

    Tensor tmpA = A, tmpB = B;

    A = Tensor(modesA, modes_extents_A, C);
    B = Tensor(modesB, modes_extents_B, F);

    // float *product = NULL;
    // Tensor c(modesC, modes_extents_C);
    // Tensor::combine(A, B, c);
    // c.getData(product);
    // for(size_t i = 0; i < elementsA * elementsB; i++) {
    //     if(matrix_copy[i] != product[i]) {
    //         cerr << "Error swapping tensors\n" << tmpA << "and\n" << tmpB;
    //         exit(EXIT_FAILURE);
    //     }
    // }
    // free(product);
    // free(matrix_copy);
    
    cudaFree(tmp_data);
    free(C);
    free(F);
}

// void TensorTrain::simplify(const Tensor& t)
// {
//     int p = train.size() - 1;
//     Tensor tmp = train[p] * t;
//     train.pop_back();
//     modes.pop_back();
//     if(train.size() > 0)
//     {
//         tmp = train[p-1] * tmp;
//         train[p-1] = move(tmp);
//     }
//     else
//     {
//         factor *= tmp.asScalar();
//     }
// }

int64_t TensorTrain::maxRank = 0;

TensorTrain::TensorTrain()
{
    factor = 1;
}

TensorTrain::TensorTrain(TensorTrain&& other) : TensorTrain()
{
    *this = move(other);
}

size_t TensorTrain::size() const
{
    return train.size();
}

void TensorTrain::add(const Tensor& t, int mode)
{
    train.emplace_back(t);
    modes.push_back(mode);
}

void TensorTrain::add(const Tensor&& t, int mode)
{
    train.emplace_back(t);
    modes.push_back(mode);
}

float TensorTrain::getEntry(const vector<int>& index) const
{
    Tensor tmp ({}, {}, new float[1]{factor});

    for(int i = 0; i < train.size(); i++)
    {
        Tensor select({modes[i]}, {2}, new float[2]{float(1 - index[i]), float(index[i])});
        tmp = (tmp * (select * train[i]));
    }

    return tmp.asScalar();
}

vector<int> TensorTrain::getModes() const
{
    return modes;
}

float TensorTrain::asScalar() const
{
    if(train.size() > 0)
    {
        throw runtime_error("TensorTrain is not a scalar.");
    }
    else
    {
        return factor;
    }
}

Tensor TensorTrain::asTensor() const {
    Tensor result({}, {}, new float[1]{factor});
    for(const Tensor& t : train) {
        result = result * t;
    }
    return result;
}

TensorTrain& TensorTrain::operator=(TensorTrain&& other)
{
    train = move(other.train);
    modes = move(other.modes);
    factor = other.factor;
    return *this;
}

TensorTrain std::ClauseTensorTrain(const vector<int>& modes)
{
    TensorTrain result;
    
    if(modes.size() == 1)
    {
        int mode = abs(modes[0]);
        Tensor t = Tensor({mode}, {2}, new float[2]{0,1});
        result.add(move(t), mode);
    }
    else if(modes.size() > 1)
    {
        int firstMode = abs(modes[0]);
        Tensor firstTensor({firstMode, linkModeCounter}, {2, 2}, new float[4]{0,1,1,1});
        result.add(move(firstTensor), firstMode);
        linkModeCounter--;

        for(int i = 1; i < modes.size() - 1; i++)
        {
            int mode = abs(modes[i]);
            Tensor t({linkModeCounter + 1, mode, linkModeCounter}, {2,2,2}, new float[8]{1,0,0,1,0,1,0,1});
            result.add(move(t), mode);
            linkModeCounter--;
        }

        int lastMode = abs(modes[modes.size()-1]);
        Tensor lastTensor({linkModeCounter + 1, lastMode}, {2, 2}, new float[4]{1,0,0,1});
        result.add(move(lastTensor), lastMode);
    }

    return result;
}

TensorTrain std::CopyTensorTrain(const vector<int>& modes)
{
    TensorTrain result;

    if(modes.size() == 1)
    {
        int mode = abs(modes[0]);
        Tensor t = Tensor({mode}, {2}, new float[2]{1,1});
        result.add(move(t), mode);
    }
    else if(modes.size() > 1)
    { 
        int firstMode = abs(modes[0]);
        Tensor firstTensor({firstMode, linkModeCounter}, {2, 2}, (modes[0] > 0) ? new float[4]{1,0,0,1} : new float[4]{0,1,1,0});
        result.add(move(firstTensor), firstMode);
        linkModeCounter--;

        for(int i = 1; i < modes.size() - 1; i++)
        {
            int mode = abs(modes[i]);
            Tensor t({linkModeCounter + 1, mode, linkModeCounter}, {2,2,2}, (modes[i] > 0) ? new float[8]{1,0,0,0,0,0,0,1} : new float[8]{0,0,1,0,0,1,0,0});
            result.add(move(t), mode);
            linkModeCounter--;
        }

        int lastMode = abs(modes[modes.size() - 1]);
        Tensor lastTensor({linkModeCounter + 1, lastMode}, {2, 2}, (modes[modes.size() - 1] > 0) ? new float[4]{1,0,0,1} : new float[4]{0,1,1,0});
        result.add(move(lastTensor), lastMode);
    }

    return result;
}

vector<int> std::getCommonModes(const TensorTrain& A, const TensorTrain& B)
{
    vector<int> commonModes;
    unordered_set<int> modesA(A.modes.begin(), A.modes.end());
    for(auto m : B.modes)
    {
        if(modesA.find(m) != modesA.end())
        {
            commonModes.push_back(m);
        }
    }
    return commonModes;
}

ostream& std::operator<<(ostream& out, const TensorTrain& t)
{
    out << "Train has " << t.train.size() << " tensors. Modes are (";
    for(auto m : t.modes)
    {
        out << m << " ";
    }
    out << "\b)\n";
    for(int i = 0; i < t.train.size(); i++)
    {
        out << "Tensor " << i << endl << t.train[i] << endl;
    }
    return out;
}

TensorTrain std::operator*(const TensorTrain& A, const TensorTrain& B)
{
    vector<int> commonModes = getCommonModes(A, B);

    TensorTrain tmpA(A), tmpB(B);

    // cerr << "Modes of A:";
    // for(auto m : A.modes) cerr << " " << m;
    // cerr << endl;
    // cerr << "Modes of B:";
    // for(auto m : B.modes) cerr << " " << m;
    // cerr << endl;

    // cerr << "Moving modes:";
    // for(auto m : commonModes) cerr << " " << m;
    // cerr << endl;
    // cerr << "To send...\n";

    TensorTrain::ThreadParams paramsA {&tmpA, &commonModes, true};
    TensorTrain::ThreadParams paramsB {&tmpB, &commonModes, true};

    pthread_t threadA, threadB;

    auto start = chrono::high_resolution_clock::now();

    if(pthread_create(&threadA, NULL, move_to_end, (void*) &paramsA)
    || pthread_create(&threadB, NULL, move_to_front, (void*) &paramsB)) {
        cerr << "Error creating a thread.\n";
        exit(EXIT_FAILURE);
    }

    while(paramsA.running || paramsB.running) {
        auto currentTime = chrono::high_resolution_clock::now();
        auto mins = chrono::duration_cast<chrono::minutes>(currentTime - start);
        if(mins.count() >= 5) {
            pthread_cancel(threadA);
            pthread_cancel(threadB);
            throw TimeoutException();
        }
    }
    Tensor *xa, *xb;
    pthread_join(threadA, (void**) &xa);
    pthread_join(threadB, (void**) &xb);

    // Tensor xa = tmpA.move_to_end(commonModes);
    // cerr << "\"xa\" tensor is:\n" << xa;
    // cerr << "To front...\n";
    // Tensor xb = tmpB.move_to_front(commonModes);
    // cerr << "\"xb\" tensor is:\n" << xb;
    Tensor middle = *xa * *xb;
    free(xa);
    free(xb);

    // cerr << "\"Middle\" tensor is:\n" << middle;

    if(tmpA.size() > 0) {
        tmpA.train[tmpA.size() - 1] = tmpA.train[tmpA.size() - 1] * middle;
    }
    else if(tmpB.size() > 0) {
        tmpB.train[0] = tmpB.train[0] * middle;
    }
    else {
        tmpA.factor *= middle.asScalar();
    }

    tmpA.factor *= tmpB.factor;
    tmpA.train.insert(tmpA.train.end(), make_move_iterator(tmpB.train.begin()), make_move_iterator(tmpB.train.end()));
    tmpA.modes.insert(tmpA.modes.end(), tmpB.modes.begin(), tmpB.modes.end());

    return tmpA;
}

void* std::move_to_end(void* void_ptr)
{
    TensorTrain::ThreadParams* params = (TensorTrain::ThreadParams*) void_ptr;
    TensorTrain* tensor_train = params->tt;
    const vector<int>& modes = *params->modes;

    Tensor* result = new Tensor({}, {}, new float[1]{1});
    
    Tensor& acc = *result;
    size_t deleted = 0;
    for(size_t i = 0; i < tensor_train->modes.size(); i++) {
        if(find(modes.begin(), modes.end(), tensor_train->modes[i]) != modes.end()) {
            // The current tensor holds a mode that will be contracted
            // So it is accumulated in acc and it's removed from the vector
            acc = acc * tensor_train->train[i];
            // cerr << "ACC is: " << acc;
            // Increment the number of "deleted" tensors so it's easy to shift the remaining tensors
            deleted ++;
        }
        else if(deleted > 0) { // If no deletion occurred there's nothing to be done
            // This tensor won't be touched so it is swapped with the accumulator and shifted in the next free position
            // cerr << "Pre product: " << acc * this->train[i];
            TensorTrain::swap(acc, tensor_train->train[i]);
            // cerr << "Post product: " << acc * this->train[i];
            tensor_train->modes[i - deleted] = tensor_train->modes[i];
            tensor_train->train[i - deleted] = tensor_train->train[i];
        }
    }

    tensor_train->train.erase(tensor_train->train.end() - deleted, tensor_train->train.end());
    tensor_train->modes.erase(tensor_train->modes.end() - deleted, tensor_train->modes.end());

    params->running = false;

    pthread_exit((void *) result);
}

void* std::move_to_front(void* void_ptr)
{
    TensorTrain::ThreadParams* params = (TensorTrain::ThreadParams*) void_ptr;
    TensorTrain* tensor_train = params->tt;
    const vector<int>& modes = *params->modes;

    Tensor* result = new Tensor({}, {}, new float[1]{1});
    
    Tensor& acc = *result;
    size_t deleted = 0;
    for(size_t i = tensor_train->modes.size() - 1; i != SIZE_MAX; i--) { // Note that i is compared to SIZE_MAX because since it is unsigned can't go below 0
        if(find(modes.begin(), modes.end(), tensor_train->modes[i]) != modes.end()) {
            // The current tensor holds a mode that will be contracted
            // So it is accumulated in acc and it's removed from the vector
            acc = tensor_train->train[i] * acc;
            // Increment the number of "deleted" tensors so it's easy to shift the remaining tensors
            deleted ++;
        }
        else if(deleted > 0) { 
            // This tensor won't be touched so it is shifted in the next free position
            TensorTrain::swap(tensor_train->train[i], acc);
            tensor_train->modes[i + deleted] = tensor_train->modes[i];
            tensor_train->train[i + deleted] = tensor_train->train[i];
        }
    }

    tensor_train->train.erase(tensor_train->train.begin(), tensor_train->train.begin() + deleted);
    tensor_train->modes.erase(tensor_train->modes.begin(), tensor_train->modes.begin() + deleted);

    params->running = false;

    pthread_exit((void *) result);
}
