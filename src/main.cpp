#include "tensor_train.h"
#include "exception.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <cstdlib>
#include <cassert>
#include <algorithm>
#include <chrono>

using namespace std;

int clauses, variables;

vector<TensorTrain> parseInput(istream& in)
{
    vector<TensorTrain> result;
    while(in.get() == 'c')
    {
        while(in.get() != '\n') {}
    }
    string format;
    in >> format;
    assert(format == "cnf");
    in >> variables >> clauses;

    fprintf(stderr, "Parsing input with %d variables in %d clauses.\n", variables, clauses);

    unordered_map<int, vector<int>> indexes;

    int index = 1;

    for(int i = 0; i < clauses; i++)
    {
        int x;
        in >> x;
        vector<int> modes;
        while(x != 0)
        {
            modes.push_back(index);
            if(x > 0) indexes[x].push_back(index);
            else indexes[-x].push_back(-index);
            index ++;
            in >> x;
        }
        
        // fprintf(stderr, "Creating clause tensor with %lu modes:", modes.size());
        // for(const auto& m : modes) fprintf(stderr, " %d", m);
        // cerr << endl;

        result.emplace_back(ClauseTensorTrain(modes));
    }

    int count = 0;
    for(const auto& x : indexes)
    {
        // fprintf(stderr, "Creating copy tensor with %lu modes:", x.second.size());
        // for(const auto& m : x.second) fprintf(stderr, " %d", m);
        // cerr << endl;

        result.emplace_back(CopyTensorTrain(x.second));
        if(x.second.size() == 1)
        {
            count++;
        }
    }
    fprintf(stderr, "Number of isolated variables: %d\n", count);

    return result;
}

int main(int argc, char** argv)
{
    if(argc != 2) {
        cout << "You must provide an input file\n";
        exit(EXIT_FAILURE);
    }

    cout << "--- Reading from file " << argv[1] << " ---\n";

    ifstream in(argv[1]);
    vector<TensorTrain> input = parseInput(in);
    in.close();

    cutensorCreate(&Tensor::handle);

    // cerr << "Input tensors orders are:";
    // for(const TensorTrain& t : input) {
    //     fprintf(stderr, " %lu", t.size());
    // }
    // cerr << endl;

    auto start = chrono::high_resolution_clock::now();

    while(input.size() > 1)
    {
        auto currentTime = chrono::high_resolution_clock::now();
        auto mins = chrono::duration_cast<chrono::minutes>(currentTime - start);
        if(mins.count() >= 15) {
            break;
        }

        int a;
        int b;
        size_t minSum = INT_MAX;
        float biggestRatio = 0;

        for(int i = 0; i < input.size(); i++)
        {
            for(int j = i+1; j < input.size(); j++)
            {
                size_t sizeSum = input[i].size() + input[j].size();
                size_t removedModes = getCommonModes(input[i], input[j]).size();
                // float newRatio = float(2 * removedModes) / float(sizeSum);
                // if(newRatio > biggestRatio)
                // {
                //     biggestRatio = newRatio;
                //     a = i;
                //     b = j;
                // }
                size_t newSum = sizeSum - 2* removedModes;
                if(newSum < minSum && removedModes > 0)
                {
                    minSum = newSum;
                    a = i;
                    b = j;
                }
            }
        }

        try { input.emplace_back(input[a] * input[b]); }
        catch(TimeoutException) {
            break;
        }

        swap(input[a], input[input.size() - 1]);
        swap(input[b], input[input.size() - 2]);
        input.resize(input.size() - 2);

        // fprintf(stderr, "There are %lu tensors left to contract.\nMax rank currently is %ld\n", input.size(), TensorTrain::maxRank);
    }

    auto stop = chrono::high_resolution_clock::now();
    auto sec = chrono::duration_cast<chrono::seconds>(stop - start);
    auto ms = chrono::duration_cast<chrono::milliseconds>(stop - start);

    ofstream out("results.tsv", ios_base::app);
    float value = -1;
    if(input.size() == 1) {
        try {
            value = input[0].asScalar(); 
        }
        catch(runtime_error _) {
            value = -1;
        }
    }
    
    out << argv[1] << "\t" << value << "\t" << variables << "\t" << clauses << "\t" << sec.count() << "." << ms.count() % 1000 << endl;
    out.close();

    // cerr << "DONE\n";
    // cout << "Result: " << input[0].asScalar() << endl;
    fprintf(stderr, "Maximum rank was %ld\n", TensorTrain::maxRank);
    // fprintf(stderr, "Elapsed time: %ld minutes\n", minutes.count());
    // fprintf(stderr, "Elapsed time: %ld hours\n", hours.count());

    cout << "--- Input " << argv[1] << " done ---\n";

    return 0;
}
