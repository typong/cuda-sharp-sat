#include "tensor_train.h"
#include "tensor.h"
#include "matrix.h"
#include <iostream>
#include <cutensor.h>
#include <cstdlib>
#include <cassert>
#include <random>

using namespace std;

default_random_engine generator;
uniform_real_distribution<float> f_distribution(-100, 100);
uniform_int_distribution<int> i_distribution(2, 10);

float random_float() {
    return f_distribution(generator);
}

int random_int() {
    return i_distribution(generator);
}

int main() {

    cutensorCreate(&Tensor::handle);

    Tensor i({}, {}, new float[1]{1});

    float data_a[8] = {1, 0, 0, 0, 0, 0, 0, 1};
    Tensor a({-209, 164, -210}, {2, 2, 2}, data_a);

    float data_b[12] = {1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1};
    Tensor b({-210, 64, -43}, {2, 2, 3}, data_b);

    cout << a * b;

    TensorTrain::swap(a, b);

    Tensor c({-209, 164, 64, -43}, {2, 2, 2, 3});
    Tensor::combine(a, b, c);

    cout << c;

    // Tensor b1({-370, 260, 67, 261, 78}, {2, 2, 2, 2, 2});

    // Tensor::combine(i, b, b1);
    // cout << a << b1;

    // cout << a * b1;
    // vector<int> modes({-166, -370});
    // vector<int64_t> extents({2, 2});
    // Tensor c(modes, extents);

    // Tensor::combine(a, b, c);
    // cout << c;

    // float *data = (float*) Tensor::combine(a, b, modes, extents);
    // float *C, *F;
    // int rank = rankFactorization(data, 8, 2, C, F);
    // cout << rank << endl;

    // cout << "C:\n";
    // printMatrix(C, 8, rank);
    // cout << "F:\n";
    // printMatrix(F, rank, 2);

    return 0;
}
