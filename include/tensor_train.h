#pragma once
#include "tensor.h"
#include <vector>

namespace std
{
    class TensorTrain
    {
    private:
        vector<Tensor> train;
        vector<int> modes;
        float factor;

        struct ThreadParams {
            TensorTrain* tt;
            const vector<int>* modes;
            bool running;
        };

        // void simplify(const Tensor& t);
    public:
        static int64_t maxRank;
        TensorTrain();
        TensorTrain(const TensorTrain& other) = default;
        TensorTrain(TensorTrain&& other);
        ~TensorTrain() = default;

        size_t size() const;
        void add(const Tensor& t, int mode);
        void add(const Tensor&& t, int mode);
        float getEntry(const vector<int>& index) const;
        vector<int> getModes() const;
        float asScalar() const;
        Tensor asTensor() const;

        TensorTrain& operator=(TensorTrain&& other);

        static void swap(Tensor& A, Tensor& B);

        friend vector<int> getCommonModes(const TensorTrain& A, const TensorTrain& B);
        friend ostream& operator<< (ostream& out, const TensorTrain& t);
        friend TensorTrain operator* (const TensorTrain& A, const TensorTrain& B);
        friend void* move_to_end(void* void_ptr);
        friend void* move_to_front(void* void_ptr);
    };

    vector<int> getCommonModes(const TensorTrain& A, const TensorTrain& B);
    ostream& operator<< (ostream& out, const TensorTrain& t);
    TensorTrain operator* (const TensorTrain& A, const TensorTrain& B);

    void* move_to_end(void* void_ptr);
    void* move_to_front(void* void_ptr);

    TensorTrain ClauseTensorTrain(const vector<int>& modes);
    TensorTrain CopyTensorTrain(const vector<int>& modes);
}
