#pragma once
#include <stdexcept>

namespace std
{
    class TimeoutException : public runtime_error
    {
    public:
        TimeoutException() : runtime_error("The code has timed out.") {};
        ~TimeoutException() {};
    };
}
