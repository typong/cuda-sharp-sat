namespace std
{
    void printMatrix(float *A, size_t n, size_t m);
    __host__ size_t reducedRowEchelon(float *A, size_t n, size_t m);
    __host__ size_t rankFactorization(float *A, size_t n, size_t m, float *&C, float *&F);
    __global__ void vectorSum(void* v, int n, void* result);
}
