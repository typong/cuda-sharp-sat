#pragma once
#include <vector>
#include <cutensor.h>
#include <iostream>

namespace std
{
    class Tensor
    {
    private:
        float *host_data;
        vector<int> modes;
        vector<int64_t> modes_extents;
        static const cutensorDataType_t inputType;
        static const cutensorComputeDescriptor_t computeType;
    protected:
        void initData(const float* data);

    public:
        Tensor(const vector<int>& modes, const vector<int64_t>& modes_extents);
        Tensor(const vector<int>& modes, const vector<int64_t>& modes_extents, const float *data);
        Tensor(const Tensor&);
        Tensor(Tensor&&);
        Tensor();
        ~Tensor();

        static cutensorHandle_t handle;

        Tensor& operator=(const Tensor& other);
        Tensor& operator=(Tensor&& other);
        
        vector<int> getModes() const;
        int getMode(size_t i) const;
        vector<int64_t> getModesExtents() const;
        int64_t getModeExtent(size_t i) const;
        size_t getDataSize() const;
        size_t getElementsCount() const;
        size_t getModesCount() const;
        void getData(float *&buff) const;
        float asScalar() const;

        cutensorTensorDescriptor_t getDescriptor(const cutensorHandle_t& handle) const;

        static vector<int> getCombinedModes(const Tensor& A, const Tensor& B);
        static Tensor combine(const Tensor& A, const Tensor& B);
        static void combine(const Tensor& A, const Tensor& B, Tensor& C);
        static void* combine(const Tensor& A, const Tensor& B, vector<int>& modesC, vector<int64_t>& extentsC);
        static void* combine(const cutensorHandle_t& handle, const Tensor& A, const Tensor& B, vector<int>& modesC, vector<int64_t>& extentsC);
        Tensor operator*(const Tensor& other) const;

        friend ostream& operator<< (ostream& out, const Tensor& t);
    };

    ostream& operator<< (ostream& out, const Tensor& t);
}
