#!bin/bash

printf "File\tResult\tVariables\tClauses\tTime\n" > results.tsv
i=0
for CLAUSES in $(seq 10 50)
do
    for iter in $(seq 1 50)
    do
        MIN_VARS=$(((CLAUSES - 1) / 4 + 1))
        MAX_VARS=$(((CLAUSES * 2 + 1) / 3 - 1))
        VARS=$((MIN_VARS + RANDOM % (MAX_VARS - MIN_VARS)))
        printf "Generating input %d with %d variables and %d clauses.\n" $i $VARS $CLAUSES
        ../Power-Law-Random-SAT-Generator/CreateSAT -g u -u 1 -q -v $VARS -c $CLAUSES -f inputs/$i
        LD_LIBRARY_PATH=`pwd`/libcutensor/lib/12 bin/main inputs/$i.cnf
        i=$((i + 1))
    done
done
