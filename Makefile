NVCC = nvcc
NVCC_FLAGS = -gencode arch=compute_89,code=sm_89 -G -g -std=c++11 -Iinclude -Ilibcutensor/include -Xcompiler="-pthread" --default-stream per-thread
LINK = nvcc
LINK_FLAGS = -gencode arch=compute_89,code=sm_89 -G -g -Llibcutensor/lib/12 -lcutensor

SOURCES = $(shell find src/ -type f -name "*.cu")
OBJ = ${subst src/, build/, ${SOURCES:.cu=.o}}

# VPATH = src:.

.PHONY: default run clean info main test

.PRECIOUS: build/%.o

main: bin/main

test: bin/test
	# LD_LIBRARY_PATH=`pwd`/libcutensor/lib/12 compute-sanitizer --tool memcheck bin/test
	LD_LIBRARY_PATH=`pwd`/libcutensor/lib/12 bin/test

build/%.o: src/%.cu
	mkdir -p $(@D)
	${NVCC} ${NVCC_FLAGS} -dc $< -o $@

build/%.o: src/%.cpp
	mkdir -p $(@D)
	${NVCC} -x c++ ${NVCC_FLAGS} -dc $< -o $@

bin/%: build/%.o ${OBJ}
	mkdir -p $(@D)
	${LINK} ${LINK_FLAGS} $^ -o $@

run:
	LD_LIBRARY_PATH=`pwd`/libcutensor/lib/12 bin/main

clean:
	rm -rf bin build

info:
	@echo ${SOURCES}
	@echo ${OBJ}
